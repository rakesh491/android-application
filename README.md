# Android applications:
##To download the repository click on "Downloads" link on the left panel of this page.
[snapshot](https://i.imgur.com/wyeEjMM.png)
***

##All the three applications are safe to run on any android device.

# 1. Bosh_rexroth.apk

1. This android application was made by me for the faculties of C.V Raman College, Bhubaneshwar.
2. The application uses simple components like activities and web view.
3. This application was created to simplify the tedious task of opening websites on the browser and then entering data.
4. Through this app all the necessary links are at one place to make task easier.

## Note

1. Min required API Level: 21
2. The links in the apps might take few moments to load.

***
# 2. Bosh_rexroth_pneumatics.apk

1. This android application was created to provide Bosh Rexroth manual material.

## Note

1. Min required API Level: 21
2. The app might not run on few android devices.

***
# 3. downloader_App.apk

1. This was an experimental project to test how android apps could download files from the URL.
2. The app uses simple concepts of networking, I/O stream and Files.

## Note

1. Min required API Level: 21
2. To dowload a file, paste the complete URL of it along with the protocol in the text field.
3. The "go to download directory" functions uses ESexplorer to open the download directory. If ESexplorer is not found the app will redirect to GooglePlay Store to donload it.
